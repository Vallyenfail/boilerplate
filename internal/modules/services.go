package modules

import (
	"gitlab.com/Vallyenfail/BoilerPlate/internal/infrastructure/component"
	aservice "gitlab.com/Vallyenfail/BoilerPlate/internal/modules/auth/service"
	uservice "gitlab.com/Vallyenfail/BoilerPlate/internal/modules/user/service"
	"gitlab.com/Vallyenfail/BoilerPlate/internal/storages"
)

type Services struct {
	User uservice.Userer
	Auth aservice.Auther
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	userService := uservice.NewUserService(storages.User, components.Logger)
	return &Services{
		User: userService,
		Auth: aservice.NewAuth(userService, storages.Verify, components),
	}
}
