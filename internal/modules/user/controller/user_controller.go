package controller

import (
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi/v5"
	"github.com/ptflp/godecoder"
	"gitlab.com/Vallyenfail/BoilerPlate/internal/infrastructure/component"
	"gitlab.com/Vallyenfail/BoilerPlate/internal/infrastructure/errors"
	"gitlab.com/Vallyenfail/BoilerPlate/internal/infrastructure/handlers"
	"gitlab.com/Vallyenfail/BoilerPlate/internal/infrastructure/responder"
	"gitlab.com/Vallyenfail/BoilerPlate/internal/models"
	"gitlab.com/Vallyenfail/BoilerPlate/internal/modules/user/service"
	"io"
	"net/http"
)

type Userer interface {
	Profile(http.ResponseWriter, *http.Request)
	GetUsersInfo(http.ResponseWriter, *http.Request)
	SearchVacancy(http.ResponseWriter, *http.Request)
	GetByIDVacancy(http.ResponseWriter, *http.Request)
	DeleteVacancy(http.ResponseWriter, *http.Request)
	GetListVacancies(http.ResponseWriter, *http.Request)
}

type User struct {
	service service.Userer
	responder.Responder
	godecoder.Decoder
}

func NewUser(service service.Userer, components *component.Components) Userer {
	return &User{service: service, Responder: components.Responder, Decoder: components.Decoder}
}

func (u *User) Profile(w http.ResponseWriter, r *http.Request) {
	claims, err := handlers.ExtractUser(r)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}
	out := u.service.GetByID(r.Context(), service.GetByIDIn{UserID: claims.ID})
	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, ProfileResponse{
			ErrorCode: out.ErrorCode,
			Data: Data{
				Message: "retrieving user error",
			},
		})
		return
	}

	u.OutputJSON(w, ProfileResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: Data{
			User: *out.User,
		},
	})
}

func (u *User) GetUsersInfo(w http.ResponseWriter, r *http.Request) {
	panic("implement me")
}

func (u *User) SearchVacancy(w http.ResponseWriter, r *http.Request) {
	client := &http.Client{}
	//var bearer = "Bearer " + r.Header.Get("Authorization")
	req, err := http.NewRequest("POST", "http://localhost:8080/search", r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	//req.Header.Add("Authorization", bearer)
	_, err = client.Do(req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(200)
}

func (u *User) GetByIDVacancy(w http.ResponseWriter, r *http.Request) {
	client := &http.Client{}
	var vacancy models.Vacancy
	vacancyIDRaw := chi.URLParam(r, "id")
	//var bearer = "Bearer " + r.Header.Get("Authorization")

	req, err := http.NewRequest("GET", fmt.Sprintf("http://localhost:8080/vacancy/%s", vacancyIDRaw), nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	//req.Header.Add("Authorization", bearer)

	resp, err := client.Do(req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer resp.Body.Close()
	bodyText, err := io.ReadAll(resp.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = json.Unmarshal(bodyText, &vacancy)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	u.OutputJSON(w, vacancy)
}

func (u *User) DeleteVacancy(w http.ResponseWriter, r *http.Request) {
	client := &http.Client{}
	vacancyIDRaw := chi.URLParam(r, "id")
	//var bearer = "Bearer " + r.Header.Get("Authorization")

	req, err := http.NewRequest("DELETE", fmt.Sprintf("http://localhost:8080/vacancy/%s", vacancyIDRaw), nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	//req.Header.Add("Authorization", bearer)

	resp, err := client.Do(req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer resp.Body.Close()

	w.WriteHeader(200)
}

func (u *User) GetListVacancies(w http.ResponseWriter, r *http.Request) {
	client := &http.Client{}
	var vacancy []models.Vacancy
	//var bearer = "Bearer " + r.Header.Get("Authorization")

	req, err := http.NewRequest("GET", "http://localhost:8080/vacancies", nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	//req.Header.Add("Authorization", bearer)

	resp, err := client.Do(req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer resp.Body.Close()
	bodyText, err := io.ReadAll(resp.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = json.Unmarshal(bodyText, &vacancy)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	u.OutputJSON(w, vacancy)
}
