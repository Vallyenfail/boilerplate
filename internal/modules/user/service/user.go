package service

import (
	"context"
	"github.com/lib/pq"
	"gitlab.com/Vallyenfail/BoilerPlate/internal/db/adapter"
	"gitlab.com/Vallyenfail/BoilerPlate/internal/infrastructure/errors"
	"gitlab.com/Vallyenfail/BoilerPlate/internal/infrastructure/tools/cryptography"
	"gitlab.com/Vallyenfail/BoilerPlate/internal/models"
	"gitlab.com/Vallyenfail/BoilerPlate/internal/modules/user/storage"
	"go.uber.org/zap"
)

type UserService struct {
	storage storage.Userer
	logger  *zap.Logger
}

func NewUserService(storage storage.Userer, logger *zap.Logger) *UserService {
	return &UserService{storage: storage, logger: logger}
}

func (u *UserService) Create(ctx context.Context, in UserCreateIn) UserCreateOut {
	var dto models.UserDTO
	dto.SetName(in.Name).
		SetPhone(in.Phone).
		SetEmail(in.Email).
		SetPassword(in.Password).
		SetRole(in.Role)

	userID, err := u.storage.Create(ctx, dto)
	if err != nil {
		if v, ok := err.(*pq.Error); ok && v.Code == "23505" {
			return UserCreateOut{
				ErrorCode: errors.UserServiceUserAlreadyExists,
			}
		}
		return UserCreateOut{
			ErrorCode: errors.UserServiceCreateUserErr,
		}
	}

	return UserCreateOut{
		UserID: userID,
	}
}

func (u *UserService) Update(ctx context.Context, in UserUpdateIn) UserUpdateOut {
	var dto models.UserDTO
	dto.SetName(in.User.Name).
		SetPhone(in.User.Phone).
		SetEmail(in.User.Email).
		SetPassword(in.User.Password).
		SetRole(in.User.Role).
		SetID(in.User.ID).
		SetEmailVerified(in.User.EmailVerified).
		SetVerified(in.User.Verified).
		SetPhoneVerified(in.User.PhoneVerified)

	err := u.storage.Update(ctx, dto)
	if err != nil {
		return UserUpdateOut{
			Success:   false,
			ErrorCode: errors.UserServiceUpdateErr,
		}
	}

	return UserUpdateOut{
		Success:   true,
		ErrorCode: errors.NoError,
	}

}

func (u *UserService) VerifyEmail(ctx context.Context, in UserVerifyEmailIn) UserUpdateOut {
	dto, err := u.storage.GetByID(ctx, in.UserID)
	if err != nil {
		u.logger.Error("user: GetByEmail err", zap.Error(err))
		return UserUpdateOut{
			ErrorCode: errors.UserServiceRetrieveUserErr,
		}
	}
	dto.SetEmailVerified(true)
	err = u.storage.Update(ctx, dto)
	if err != nil {
		u.logger.Error("user: update err", zap.Error(err))
		return UserUpdateOut{
			ErrorCode: errors.UserServiceUpdateErr,
		}
	}

	return UserUpdateOut{
		Success: true,
	}
}

func (u *UserService) ChangePassword(ctx context.Context, in ChangePasswordIn) ChangePasswordOut {
	dto, err := u.storage.GetByID(ctx, in.UserID)
	if err != nil {
		u.logger.Error("user: GetByID err", zap.Error(err))
		return ChangePasswordOut{ErrorCode: errors.UserServiceRetrieveUserErr}
	}

	if !cryptography.CheckPassword(dto.GetPassword(), in.OldPassword) {
		u.logger.Error("user: wrong old password")
		return ChangePasswordOut{ErrorCode: errors.AuthServiceWrongPasswordErr}
	}

	dto.SetPassword(in.NewPassword)

	err = u.storage.Update(ctx, dto)
	if err != nil {
		u.logger.Error("user: update err", zap.Error(err))
		return ChangePasswordOut{ErrorCode: errors.UserServiceUpdateErr}
	}

	return ChangePasswordOut{Success: true}
}

func (u *UserService) GetByEmail(ctx context.Context, in GetByEmailIn) UserOut {
	userDTO, err := u.storage.GetByEmail(ctx, in.Email)
	if err != nil {
		u.logger.Error("user: GetByEmail err", zap.Error(err))
		return UserOut{
			ErrorCode: errors.UserServiceRetrieveUserErr,
		}
	}

	return UserOut{
		User: &models.User{
			ID:            userDTO.GetID(),
			Name:          userDTO.GetName(),
			Phone:         userDTO.GetPhone(),
			Email:         userDTO.GetEmail(),
			Password:      userDTO.GetPassword(),
			Role:          userDTO.GetRole(),
			Verified:      userDTO.Verified,
			EmailVerified: userDTO.EmailVerified,
			PhoneVerified: userDTO.PhoneVerified,
		},
	}
}

func (u *UserService) GetByPhone(ctx context.Context, in GetByPhoneIn) UserOut {
	userDTO, err := u.storage.GetByFilter(ctx, adapter.Condition{
		Equal: map[string]interface{}{
			"phone": in.Phone}})
	if err != nil {
		u.logger.Error("user: GetByPhone err", zap.Error(err))
		return UserOut{ErrorCode: errors.UserServiceWrongPhoneCodeErr}
	}

	return UserOut{
		User: &models.User{
			ID:            userDTO[0].GetID(),
			Name:          userDTO[0].GetName(),
			Phone:         userDTO[0].GetPhone(),
			Email:         userDTO[0].GetEmail(),
			Password:      userDTO[0].GetPassword(),
			Role:          userDTO[0].GetRole(),
			Verified:      userDTO[0].Verified,
			EmailVerified: userDTO[0].EmailVerified,
			PhoneVerified: userDTO[0].PhoneVerified,
		},
	}
}

func (u *UserService) GetByID(ctx context.Context, in GetByIDIn) UserOut {
	userDTO, err := u.storage.GetByID(ctx, in.UserID)
	if err != nil {
		u.logger.Error("user: GetByEmail err", zap.Error(err))
		return UserOut{
			ErrorCode: errors.UserServiceRetrieveUserErr,
		}
	}

	return UserOut{
		User: &models.User{
			ID:            userDTO.GetID(),
			Name:          userDTO.GetName(),
			Phone:         userDTO.GetPhone(),
			Email:         userDTO.GetEmail(),
			Password:      userDTO.GetPassword(),
			Role:          userDTO.Role,
			Verified:      userDTO.Verified,
			EmailVerified: userDTO.EmailVerified,
			PhoneVerified: userDTO.PhoneVerified,
		},
	}
}

func (u *UserService) GetByIDs(ctx context.Context, in GetByIDsIn) UsersOut {
	var users []models.User
	usersDTO, err := u.storage.GetByIDs(ctx, in.UserIDs)
	if err != nil {
		u.logger.Error("users: GetByIDs err", zap.Error(err))
		return UsersOut{ErrorCode: errors.UserServiceRetrieveUserErr}
	}

	for _, us := range usersDTO {
		user := &models.User{
			ID:            us.GetID(),
			Name:          us.GetName(),
			Phone:         us.GetPhone(),
			Email:         us.GetEmail(),
			Password:      us.GetPassword(),
			Role:          us.Role,
			Verified:      us.Verified,
			EmailVerified: us.EmailVerified,
			PhoneVerified: us.PhoneVerified,
		}

		users = append(users, *user)
	}

	return UsersOut{User: users}
}
