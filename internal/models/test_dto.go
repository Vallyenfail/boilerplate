package models

import (
	"gitlab.com/Vallyenfail/BoilerPlate/internal/infrastructure/db/types"
	"time"
)

//go:generate easytags $GOFILE json,db,db_ops,db_type,db_default,db_index

type TestDTO struct {
	ID             int              `json:"id" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null" db_ops:"id" db_index:"id"`
	TestNullString types.NullString `json:"test_null_string" db:"test_null_string" db_type:"varchar(144)" db_default:"default null" db_ops:"create,update"`
	IntField       int              `json:"int_field" db:"int_field" db_type:"int" db_default:"default 0" db_ops:"create,update"`
	IntNull        types.NullInt64  `json:"int_null" db:"int_null" db_type:"int" db_default:"default null" db_ops:"create,update"`
	BoolField      bool             `json:"bool_field" db:"bool_field" db_type:"bool" db_default:"default false" db_ops:"create,update"`
	CreatedAt      time.Time        `json:"created_at" db:"created_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
	UpdatedAt      time.Time        `json:"updated_at" db:"updated_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
	DeletedAt      types.NullTime   `json:"deleted_at" db:"deleted_at" db_type:"timestamp" db_default:"default null" db_index:"index"`
}

func (u *TestDTO) TableName() string {
	return "test"
}

func (u *TestDTO) OnCreate() []string {
	return []string{}
}
