package router

import (
	"github.com/go-chi/chi/v5"
	"gitlab.com/Vallyenfail/BoilerPlate/internal/infrastructure/component"
	"gitlab.com/Vallyenfail/BoilerPlate/internal/infrastructure/middleware"
	"gitlab.com/Vallyenfail/BoilerPlate/internal/modules"
	"net/http"
)

func NewApiRouter(controllers *modules.Controllers, components *component.Components) http.Handler {
	r := chi.NewRouter()
	userController := controllers.User
	authCheck := middleware.NewTokenManager(components.Responder, components.TokenManager)
	authController := controllers.Auth

	r.Route("/api", func(r chi.Router) {
		r.Route("/1", func(r chi.Router) {

			r.Route("/auth", func(r chi.Router) {

				r.Post("/register", authController.Register)
				r.Post("/login", authController.Login)
				r.Route("/refresh", func(r chi.Router) {
					r.Use(authCheck.CheckRefresh)
					r.Post("/", authController.Refresh)
				})
				r.Post("/verify", authController.Verify)
			})
			r.Route("/user", func(r chi.Router) {
				r.Route("/profile", func(r chi.Router) {
					r.Use(authCheck.CheckStrict)
					r.Get("/", userController.Profile)
				})
			})
		})
	})

	r.Route("/vacancy", func(r chi.Router) {
		r.Use(authCheck.CheckStrict)
		r.Delete("/{id}", userController.DeleteVacancy)
		r.Get("/{id}", userController.GetByIDVacancy)
	})
	r.Post("/search", userController.SearchVacancy)
	r.Get("/vacancies", userController.GetListVacancies)

	return r
}
